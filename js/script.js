var TitreDuSwag = "Clique pour générer un titre";
var firstsyllabe = ["RA", "BLAN", "AL", "PE", "JOY", "BI", "FRIN", "FU", "JO", "MÉ", "TU", "CUR", "CAL", "TÉ", "SI", "IM", "TU", "PIS", "AL"];
var secondsyllabe = ["TA", "KA", "GANT", "DU", "ÉA", "JEC", "TIF", "TIT", "BE", "QUETTE", "EUX", "RY", "CU", "TRA", "NU", "TAS", "PO", "ILE", "TIL", "GO"];
var thirdsyllabe = ["RI", "TOIRE", "TION", "TOUILLE", "SE", "LETTE", "ÈDRE", "IDALE", "STROPHE", "SITION", "LE"];
var colorpalette = ["#3d61e9","#3ee8aa","#ff485e","#fff352","#ffd6da"];

function pickColor(){
	return colorpalette[Math.floor(Math.random()*colorpalette.length)];
}

function setColor(background,font){
	if (background == font) {
	setColor(pickColor(),pickColor());
	}else{
		console.log(background);
		document.body.style.backgroundColor = background;
		document.getElementById('titre').style.color = font;
	}
}

window.onclick = function(){generate()};

function generate(){
	document.getElementById('titre').innerHTML = firstsyllabe[Math.floor(Math.random()*firstsyllabe.length)]+secondsyllabe[Math.floor(Math.random()*secondsyllabe.length)]+thirdsyllabe[Math.floor(Math.random()*thirdsyllabe.length)];
	setColor(pickColor(),pickColor());
}
